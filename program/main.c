////////////////////////////////////////////////////////////////
//ALGORYTM BELLMANA-FORDA Z WYKORZYSTANIEM MACIERZY SĄSIEDZTWA//
////////////////////////////////////////////////////////////////
//KODY WYJSCIA:
//0 - w porządku
//2 - złe argumenty
//61 - wierzchołek startowy poza zakresem numerów wierzchołków
//7 - nieudany malloc
//8 - nie udalo sie otworzyc pliku wejsciowego
//81 - w pliku nie znaleziono liczby wierzchołków (czyli linii #V=<liczba wierzchołków>)
//82 - niespójność w liście sąsiedztwa
//9 - nie udało sie otworzyć pliku wyjściowego

///FORMAT WEJŚCIOWY:
/*przykład:
#jakaś
#ilość
#komentarzy
#ale wśród nich jedna linia
#V=3
#oznaczająca ilość wierzchołków w grafie
#a potem lista sąsiedztwa w formacie : numer początkowego węzła, końcowego, waga, np:
0 1 450
0 2 280
2 0 -300
*/
/*drugi przykład:
#V=4
0 1
1 2
2 3
3 2
*/
//można opuścic wagę krawędzi, wtedy zostanie ustawiona na DOMYSLNA_WAGA_KRAWEDZI
/////////////////////////////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>

typedef int waga;//waga danego wezla
#define DOMYSLNA_WAGA_KRAWEDZI 1
#define NIE_MA 0
//#define NIE_MA (INT_MIN/2)
#define WAGA_MAX (INT_MAX/2)

typedef int nwezla;//numer wezla

struct wynik_belmana_forda{
    int czy;//są ujemne cykle
    int n;//ilosc wezlow
    waga * d;//koszt dojscia do danego wezla
    nwezla *p;//numer poprzednika na najkrotszej sciezce
};
/////////////////////////////////////////////////
//OBSŁUGA PAMIĘCI
void * mallocspr(size_t rozm)
{
    void * wsk = malloc(rozm);
    if(!wsk)
    {
        perror("NIE DOSTALEM PAMIECI - konczenie");
        exit(7);
    }
    return wsk;
}

//w tablicy ma istnieć jeszcze o zgrozo kolumna -1 (ze względu na tablicę skoki - trzeba znać indeks pierwszego niepustego elementu w danym wierszu)
waga ** daj_macierz_nxn(int n)
{
    waga ** m = (waga**)mallocspr(n*sizeof(waga*));
    for(int i=0; i<n;i++)
        m[i]=((waga*)mallocspr((n+1)*sizeof(waga))+1);//(z 1-ego elemntu robimy 0-y)
    return m;
}

void zwolnij_nxn(waga ** m, int n)
{
    for(int i=0; i<n;i++) free(m[i]-1);
    free(m);
}

void pisz_nxn(waga ** t, int n, FILE * wy)
{
    for(int i=0; i<n;i++)
    {
        for(int j=-1; j<n;j++)
        {
            fprintf(wy, "%d ", t[i][j]);
        }
        fputc('\n', wy);
    }
}

//
int czy_nxn_symetryczna(waga ** t, int n)
{
    for(int i=0; i<n;i++)
    {
        for(int j=0; j<n;j++)
        {
            if(t[i][j]!=t[j][i])return 0;
        }
    }
    return 1;
}

void pisz_nxn_w_DOT(waga ** t, int n, FILE * wy, char * nazwa)
{
    int di = !czy_nxn_symetryczna(t,n);
    if(di)fprintf(wy,"di");
    fprintf(wy,"graph %s {\n", nazwa);
    for(int i=0; i<n;i++)
    {
        for(int j=0; j<=i;j++)
        {
            if(!di && (t[i][j]!=NIE_MA))fprintf(wy, "%d -- %d;\n", i, j);
            else{
                if(t[i][j]!=NIE_MA)
                {
                    fprintf(wy, "%d -> %d", i,j);
                    if(t[i][j]!=DOMYSLNA_WAGA_KRAWEDZI)fprintf(wy, "[ label = \"%d\" ]", t[i][j]);
                    fprintf(wy,";\n");
                }
                if(t[j][i]!=NIE_MA)
                {
                    fprintf(wy, "%d -> %d", j,i);
                    if(t[j][i]!=DOMYSLNA_WAGA_KRAWEDZI)fprintf(wy, "[ label = \"%d\" ]", t[j][i]);
                    fprintf(wy,";\n");
                }
            }
        }
    }
    fprintf(wy,"}");
}
//////////////////////////////////////////////////////////////

static const char NAGLOWEK[] = "#V=";
#define STRLEN(s) ((sizeof(s)/sizeof(s[0])) - sizeof(s[0]))

//poniższy potworek ma za zadanie złapać w początkowych partiach pliku (przed listą wierzchołków) linię #V=<ilość wierzchołków> i zwrócić tę liczbę
//jest na tyle uprzejmy, że pozwala na inne komentarze zaczynające się od #
//(de facto zwraca strumien ustawiony na pierwszą linię nie zaczynającą się od #, po tym jak zczytał liczbę > 0)
int zczytaj_liczbe_wezlow(FILE * we)
{
    char c;
    int n=0;
    char buf[256];
    while(((c=getc(we))>0))
    {
        ungetc(c,we);
        if(n<=0 || c=='#')//czyli jeszce nie znaleziono liczby, albo wypadałoby pominąc kolejne linie koentarzy
        {
            fgets(buf,255,we);
            if(strncmp(NAGLOWEK, buf, STRLEN(NAGLOWEK)) == 0 )
            {
                sscanf(buf+STRLEN(NAGLOWEK), "%d", &n);
            }
        }
        else break;
    }
    if(n<=0){exit(81);}
    return n;
}

//PONIŻEJ WCZYTYWANIE ZE STRUMIENIA W FORMACIE

//0 1
//0 2
//1 2

//co ma znaczyc: z wezla 0 krawedzie do 1,2 ; z wezla 1 krawedz do 2
//do MACIERZY SĄSIEDZTWA sd która ma postać
//sd[w][k] = a <==> istnieje krawędź o wadze a od wezla w-ego do wezla k-ego
//WĘZŁY ZACZYNAJĄ SIĘ OD ZEROWEGO
//(kończy przerywając program, jeśli z linii nie zdoła przeczytac dwóch dodatnich liczb, kończy zwyczajnie napotykając koniec pliku, lub znaki ETX, ETC)
int wczytaj_wezly(waga ** sd, int n, FILE * we)
{
    //czyszczenie
    for(int i=0; i<n;i++)
        for(int j=-1; j<n;j++)
            sd[i][j]=NIE_MA;

    int c, poczatek, koniec, waga_krawedzi, argc;
    char buf[2048];//na linię
    while((c=fgetc(we))!=EOF)
    {
        if(c=='\3'|| c=='\4')break;//ASCII ETX, EOT
        if(c!='#' && c!='\n' && c!=' ')
        {
            ungetc(c, we);
            poczatek=koniec=-1;
            fgets(buf, 2047, we);
            argc = sscanf(buf, "%d %d %d", &poczatek, &koniec, &waga_krawedzi);

            if(argc >= 2 &&
               poczatek >= 0 && poczatek < n &&
               koniec >=0 && koniec < n)
            {sd[poczatek][koniec]=((argc==3)?(waga_krawedzi):(DOMYSLNA_WAGA_KRAWEDZI )); /*printf("sd[%d][%d]=%d\n", poczatek, koniec, (argc==3)?(waga_krawedzi):(DOMYSLNA_WAGA_KRAWEDZI ));*/}
            else exit(82);

        }
        else { fscanf(we, "%*[^\n]\n");}//powinno pominąć resztę niechcianej linii (pustej/komentarza)
    }
    return 0;
}

//aby iterujac po wezlach wychodzacych z danego nie chodzic po calym wierszu macierzy, na danym miejscu w 'skoki' ma byc odleglosc do nastepnego
int wypelnij_skoki(nwezla ** skoki, waga ** sd, int n)
{
    //czyszczenie (właściwie niepotrzebne, ale widać dzięki niemu coś przy debugowaniu)
    for(int i=0; i<n;i++)
        for(int j=0; j<n;j++)
            skoki[i][j]=NIE_MA;

    int poprzedni;
    for(int w=0; w<n;w++)
    {
        poprzedni=-1;
        for(int k=0; k<n;k++)
        {
            if(sd[w][k]!=NIE_MA)
            {
                skoki[w][poprzedni]= k - poprzedni;
                poprzedni=k;
            }
        }
        //ostatni element ma "prowadzić" poza tablicę (żeby pętla w bford się nie zawieszała...)
        skoki[w][poprzedni]=n-poprzedni;
    }
    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////
//właściwy algorytm Bellmana-Forda pracujący już dostarczonych macierzach i tablicach//
///////////////////////////////////////////////////////////////////////////////////////
#define TAK 1
#define NIE 0
int bford(int n, waga ** sd, nwezla ** skoki, waga * d, nwezla *p, nwezla start)
{
    //wypełnij tablicę kosztów dojścia - d, największą liczbą
    //wypełnij tablicę p - poprzedników -1kami (co znaczy, że nie ma poprzednika)
    for(int i=0; i<n;i++){d[i]=WAGA_MAX; p[i]=-1;}

    //koszt dojścia do startowego wierzchołka to 0 (bo już w nim jesteśmy)
    d[start]=0;

    //główna część algorytmu
    int nierobstwo;//jeśli w danym obiegu nic się nie zmieniło, to można skończyć
    for(int i=0;i<n;i++)
    {
        nierobstwo=TAK;
        for(int px=0;px<n;px++)
        {
            int x = px;//(px+start)%n;
            //dla każdego sąsiada wierzchołka x
            for(int y=-1+skoki[x][-1]; y<n; y+=skoki[x][y])
            {
                if(d[y] <= ( d[x]+ sd[x][y]) )continue;
                nierobstwo=NIE;
                d[y] = d[x]+sd[x][y];
                p[y] = x;
            }
        }
        if(nierobstwo)return 1;
    }

    //sprawdzamy istnienie ujemnego cyklu
    for(int x=0;x<n;x++)
    {
        //dla każdego sąsiada wierzchołka x:
        for(int y=-1; y<n; y+=skoki[x][y])
        {
            if(d[y] > (d[x]+sd[x][y]) ){ fprintf(stderr, "UJEMNY CYKL![%d][%d]", x,y);return 0;}
        }
    }

    return 1;
}

struct wynik_belmana_forda Bellman_Ford(FILE * we, nwezla poczatkowy)
{
    int n = zczytaj_liczbe_wezlow(we);
    if(poczatkowy>=n || poczatkowy<0)exit(61);

    //ALOKACJA PAMIECI - wymaga pozniejszego zwolnienia
    waga ** sd = daj_macierz_nxn(n);//MACIERZ SASIEDZTWA
    nwezla ** skoki = daj_macierz_nxn(n);//przyspieszenie iteracji po wierzszach - czyli wezlach wychodzacego od danego
    waga * d  = (waga*)mallocspr(n*sizeof(waga));//koszt dojscia z i-ego do poczatkowego
    nwezla* p = (nwezla*)mallocspr(n*sizeof(nwezla));//numer poprzednika na najkrotszej sciezce do poczatkowego

    wczytaj_wezly(sd, n, we);
    wypelnij_skoki(skoki, sd, n);

    int czy = !bford(n, sd, skoki, d,p, poczatkowy);

    zwolnij_nxn(sd,n);
    zwolnij_nxn(skoki,n);

    struct wynik_belmana_forda w = {czy,n,d,p};
    return w;
}


int main(int argc, char *argv[])
{
    char * plikwejsciowy=NULL;//nazwa
    char * plikwyjsciowy=NULL;
    FILE * wejscie=NULL;//deskryptor
    FILE * wyjscie=NULL;
    int wezel_poczatkowy=0;//domyślnie do węzła 0
    int naglowek_wyjscia=0;

    int opt;
    while ((opt = getopt(argc, argv, "i:o:s:h::")) != -1)
    {
       switch (opt)
       {
       case 'i':
           plikwejsciowy=optarg;
           break;
       case 'o':
           plikwyjsciowy=optarg;
           break;
       case 's':
           wezel_poczatkowy=atoi(optarg);
           break;
       case 'h':
            naglowek_wyjscia=1;
            break;
       default: /* '?' */
           fprintf(stderr, "Usage: %s -s start_vertex_number [-i inputfile] [-o outputfile]\n", argv[0]);
           exit(2);
       }
    }

    if(plikwejsciowy)
    {
        if(!(wejscie=fopen(plikwejsciowy, "r")) )exit(8);
    }else wejscie=stdin;

    if(plikwyjsciowy)
    {
        if(!(wyjscie=fopen(plikwyjsciowy, "w")) )exit(9);
    }else wyjscie=stdout;

//    fprintf(wyjscie, "ALGORYTM BELLMANA-FORDA\nCZYTAM Z %s LISTE SASIEDZTWA I OBLICZAM NAJKROTSZE SCIEZKI DO WIERZCHOLKA O NUMERZE %d\nPO CZYM PISZE NA %s", (plikwejsciowy)?(plikwejsciowy):("\bE standardowego wejscia"), wezel_poczatkowy,(plikwyjsciowy)?(plikwyjsciowy):("standardowe wyjscie\n"));
//WYWOŁANIE
    struct wynik_belmana_forda w = Bellman_Ford(wejscie, wezel_poczatkowy);
//PISANIE NA WYJŚCIE
    if(naglowek_wyjscia){fprintf(wyjscie, "wejscie=%s\nujemne_cykle=%s\n#V=%d\n", plikwejsciowy, (w.czy)?("tak"):("nie"), w.n);}
                            if(naglowek_wyjscia){fprintf(wyjscie, "d:");}     for(int i=0; i<w.n;i++)fprintf(wyjscie, "%+d ", w.d[i]);
    fprintf(wyjscie, "\n"); if(naglowek_wyjscia){fprintf(wyjscie, "p:");}     for(int i=0; i<w.n;i++)fprintf(wyjscie, "%+d ", w.p[i]);

//sprzątanie
    fclose(wejscie);
    fclose(wyjscie);

    return 0;
}
